<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | MEMBERS</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
    <style>
        .popup_import{
            border: 1px solid black;
            width: 550px;
            height: auto;
            background: white;
            border-radius: 3px;
            margin: 0 auto;
            padding: 5px;
        }

        .format{
            color: red;
        }

        #but_import{
            margin-left: 10px;
        }
    </style>
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../agenda">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../proxy">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../attendance">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../advanced">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Members</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Members</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <?php if ($role_id == 100 || $role_id == 90 || $role_id == 50){ ?>
      <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
          <div class="col-xl-6 order-xl-1">
              <div class="col-md-12 order-md-1">
                  <div class="card">
                      <div class="card-header">
                          <div class="row align-items-center">
                              <div class="col-12">
                                  <!--Message Response-->
                                  <div class="message"></div>
                              </div>
                              <div class="col-8">
                                  <h3 class="mb-0">Add Members </h3>
                              </div>
                          </div>
                      </div>
                      <div class="card-body">
                          <form class="members" enctype="multipart/form-data">
                              <div class="pl-lg-4">
                                  <div class="row p-4 bg-secondary">
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="phoneNo">Phone No</label>
                                              <input type="number" id="phoneNo" name="phoneNo" class="form-control form-control-alternative" placeholder="254700000000" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="MemberNo">Membership No</label>
                                              <input type="number" id="MemberNo" name="MemberNo" class="form-control form-control-alternative" placeholder="8363639" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="CDS_ClientID">CDS Client ID</label>
                                              <input type="text" id="CDS_ClientID" name="CDS_ClientID" class="form-control form-control-alternative" placeholder="8363639" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="full_name">Full Name</label>
                                              <input type="text" id="full_name" name="full_name" class="form-control form-control-alternative" placeholder="John Doe" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="PaymentName">Payment Name</label>
                                              <input type="text" id="PaymentName" name="PaymentName" class="form-control form-control-alternative" placeholder="MPESA" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="ID_RegCert_No">ID Number</label>
                                              <input type="number" id="ID_RegCert_No" name="ID_RegCert_No" class="form-control form-control-alternative" placeholder="123456" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="shares">Shares</label>
                                              <input type="number" id="shares" name="shares" class="form-control form-control-alternative" placeholder="100" autocomplete="off">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <input type="hidden" name="role_id" value="<?php echo $rw['role_id']; ?>">
                                      <input type="hidden" name="form" value="member">
                                      <input type="submit" class="btn btn-primary btn-submit">
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-xl-6 order-xl-2">
              <div class="row">
                  <div class="col-md-12 order-md-1">
                      <div class="card">
                          <div class="card-header">
                              <div class="row align-items-center">
                                  <div class="col-12">
                                      <!--Message Response-->
                                      <div class="statusMsg"></div>
                                  </div>
                                  <div class="col-8">
                                      <h3 class="mb-0">Upload a CSV File </h3>
                                  </div>
                              </div>
                          </div>
                          <div class="card-body">
                              <form class="members" enctype="multipart/form-data">
                                  <div class="pl-sm-4">
                                      <div class="row">
                                          <div class="col-auto">
                                              <div class="custom-file">
                                                  <input type="file" class="custom-file-input" id="importfile" name="importfile">
                                              </div>
                                          </div>
                                          <div class="col-auto">
                                              <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                              <input type="hidden" name="form" value="import">
                                              <input type="submit" class="btn btn-primary btn-submit" value="Import">
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>
                          <div class="card-footer text-center">
                              <p><b>INSTRUCTIONS:</b></p>
                              <span class="format">Phone No, Member Number, CDS Client ID, Full Names, ID No, Shares</span>
                              <p><a href="members.csv" target="_blank">Download Sample</a></p>
                          </div>
                      </div>
                  </div>
                  <div class="col-md-12 order-md-2">
                      <div class="card">
                          <div class="card-header">
                              <div class="row align-items-center">
                                  <div class="col-8">
                                      <h3 class="mb-0">Update Shares </h3>
                                  </div>
                              </div>
                          </div>
                          <div class="card-body">
                              <form class="members" enctype="multipart/form-data">
                                  <div class="pl-lg-4">
                                      <div class="row p-4 bg-secondary">
                                          <div class="col-12">
                                              <div class="form-group">
                                                  <label class="form-control-label" for="MemberNo">Member Number</label>
                                                  <select class="form-control form-control-alternative" name="MemberNo">
                                                      <option value="">Select Member Number</option>
                                                      <?php

                                                      $query3 = mysqli_query($con, "SELECT * FROM users ORDER BY created DESC");
                                                      $member = mysqli_fetch_assoc($query3);
                                                      do{ ?>
                                                          <option value="<?php echo $member['MemberNo']; ?>"><?php echo $member['MemberNo']; ?></option>
                                                      <?php } while ($member = mysqli_fetch_assoc($query3)); ?>
                                                  </select>
                                              </div>
                                          </div>
                                          <div class="col-12">
                                              <div class="form-group">
                                                  <label class="form-control-label" for="shares">Shares</label>
                                                  <input type="text" id="shares" name="shares" class="form-control form-control-alternative" placeholder="100" autocomplete="off">
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <input type="hidden" name="role_id" value="<?php echo $rw['role_id']; ?>">
                                          <input type="hidden" name="form" value="shares">
                                          <input type="submit" class="btn btn-primary btn-submit">
                                      </div>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
              <div class="row" style="padding-bottom: 20px">
                  <div class="col-xl-12">
                      <div class="card">
                          <div class="card-header">
                              <div class="row align-items-center">
                                  <div class="col-8">
                                      <h3 class="mb-0">NEWEST MEMBERS </h3>
                                  </div>
                              </div>
                          </div>
                          <div class="card-body">
                              <div class="table-responsive">
                                  <div id="mytable">
                                      <?php if (count($member5['id']) > 0){ ?>
                                          <table class="table align-items-center table-dark">
                                              <thead class="thead-light">
                                              <tr>
                                                  <th scope="col" class="sort" data-sort="phoneNo">Phone No</th>
                                                  <th scope="col" class="sort" data-sort="MemberNo">Member No</th>
                                                  <th scope="col" class="sort" data-sort="MemberNo">Full Name</th>
                                                  <th scope="col" class="sort" data-sort="CDS_ClientID">CDS Client ID</th>
                                                  <th scope="col" class="sort" data-sort="ID_RegCert_No">ID No</th>
                                                  <th scope="col" class="sort" data-sort="shares">Shares</th>
                                              </tr>
                                              </thead>
                                              <tbody class="list">
                                              <?php do{ ?>
                                                  <tr>
                                                      <td class="phoneNo"><?php echo $member5['phoneNo']; ?></td>
                                                      <td class="MemberNo"><?php echo $member5['MemberNo']; ?></td>
                                                      <td class="MemberNo"><?php echo $member5['full_name']; ?></td>
                                                      <td class="CDS_ClientID"><?php echo $member5['CDS_ClientID']; ?></td>
                                                      <td class="ID_RegCert_No"><?php echo $member5['ID_RegCert_No']; ?></td>
                                                      <td class="shares"><?php echo $member5['shares']; ?></td>
                                                  </tr>
                                              <?php } while ($member5 = mysqli_fetch_assoc($query9)); ?>
                                              </tbody>
                                          </table>
                                      <?php } ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
      <?php } else { ?>
      <div class="container-fluid mt--6">
          <div class="row">
              <h3>You dont have permission to view this page</h3>
          </div>
      </div>
      <?php } ?>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>


  <!--Form Submission-->
<script>
    $(document).ready(function (e) {
        $(".members").on('submit', function (e) {
            e.preventDefault();
            $(".message").html("<p class='alert alert-info'>Wait... Submitting data...</p>");
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".btn-submit").attr("disabled", "disabled");
                    $(".members").css("opacity", ".5");
                },
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        $('.members')[0].reset();
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                    $('.members').css("opacity","");
                    $(".btn-submit").removeAttr("disabled");
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('slow');
            }, 30000);
        });

        //Refresh table
        setInterval(function () {
            $( "#mytable" ).load( "index.php #mytable" );
        }, 5000);

    })
</script>
</body>

</html>