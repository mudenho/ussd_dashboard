<style>
    .main-content{
        min-height:100vh;
    }
    .footer{
        position: absolute;
        width: 95%;
        bottom: 10px;
        height: 0;
    }
</style>
<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
            <div class="copyright text-center  text-lg-left  text-muted">
                &copy; <script>document.write( new Date().getFullYear() );</script>
                <a href="https://www.legitimate-technology.co.ke" class="font-weight-bold ml-1" target="_blank">Legitimate Technologies</a>
            </div>
        </div>
        <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                <li class="nav-item">
                    <a href="https://www.legitimate-technology.co.ke" class="nav-link" target="_blank">Legitimate Technologies</a>
                </li>
                <li class="nav-item">
                    <a href="https://www.legitimate-technology.co.ke" class="nav-link" target="_blank">About Us</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
