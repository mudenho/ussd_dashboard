<?php
require 'agm.php';
require 'session.php';
require_once '../vendor/autoload.php';


$query1 = mysqli_query($con, "SELECT * FROM agendas ORDER BY created ASC");
$agenda = mysqli_fetch_assoc($query1);

$query6 = mysqli_query($con, "SELECT * FROM downloads ORDER BY created ASC");
$doc = mysqli_fetch_assoc($query6);

$query2 = mysqli_query($con, "SELECT * FROM users ORDER BY id ASC");
$members = mysqli_fetch_assoc($query2);
$total_members = mysqli_num_rows($query2);

$query9 = mysqli_query($con, "SELECT * FROM users ORDER BY id DESC LIMIT 5");
$member5 = mysqli_fetch_assoc($query9);

$query3 = mysqli_query($con, "SELECT * FROM proxy_users ORDER BY created DESC");
$proxy = mysqli_fetch_assoc($query3);

$query4 = mysqli_query($con, "SELECT * FROM admins ORDER BY created DESC");
$users = mysqli_fetch_assoc($query4);

$query5 = mysqli_query($con, "SELECT * FROM faq ORDER BY created DESC");
$faqs = mysqli_fetch_assoc($query5);

$query7 = mysqli_query($con, "SELECT * FROM quiz ORDER BY created DESC");
$other = mysqli_fetch_assoc($query7);

$shares = mysqli_fetch_assoc(mysqli_query($con, "SELECT SUM(shares) AS shares FROM users")); //total shares

$query8 = mysqli_query($con, "SELECT attendance.phoneNo, attendance.MemberNo, users.shares, attendance.type, users.full_name as user, proxy_users.full_name as proxy, attendance.created FROM attendance INNER JOIN users ON attendance.MemberNo = users.MemberNo LEFT JOIN proxy_users ON users.MemberNo = proxy_users.users_MemberNo ORDER BY attendance.created ASC");
$attendance = mysqli_fetch_assoc($query8);
$total_attendance = mysqli_num_rows($query8);

$total_attendance_shares = mysqli_fetch_assoc(mysqli_query($con, "SELECT SUM(users.shares) AS total FROM attendance INNER JOIN users ON attendance.MemberNo = users.MemberNo"));
$percentage = $total_attendance_shares['total'] / $shares['shares']*100;

//agenda for voting
$sql = mysqli_query($con, "SELECT agenda_name FROM `votes` GROUP BY agenda_name");
$agenda_vote = mysqli_fetch_assoc($sql);

$sql1 = mysqli_query($con, "SELECT * FROM sms");
$sms = mysqli_fetch_assoc($sql1);



function formatMobileNumber($mobile) {

    $mobile = preg_replace('/\s+/','',$mobile);

    $input1  = substr($mobile,0,-strlen($mobile) + 1);
    $input2  = substr($mobile,0,-strlen($mobile) + 2);
    $input3  = substr($mobile,0,-strlen($mobile) + 3);
    $input4  = substr($mobile,0,-strlen($mobile) + 4);
    $input5  = substr($mobile,0,-strlen($mobile) + 5);
    $length = strlen($mobile);
    $number = false;

    if ($input1 == '7' && $length == 9)
    {
        $number = substr_replace($mobile,'2547',0,1);
    }
    else if ($input2 == '07' && $length == 10 )
    {
        $number = "254" . substr($mobile, 1);
    }
    else if ($input4 == '2547' && $length == 12)
    {
        $number = $mobile;
    }
    else if ($input5 == '+2547' && $length == 13)
    {
        $number = substr_replace($mobile,'',0,1);
    }

    return $number;
}

function Unique($no){
    $Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
    $QuantidadeCaracteres = strlen($Caracteres);
    $QuantidadeCaracteres--;

    $Hash=NULL;
    for($x=1;$x<=$no;$x++){
        $Posicao = rand(0,$QuantidadeCaracteres);
        $Hash .= substr($Caracteres,$Posicao,1);
    }

    return $Hash;
}

function sendMail($receiver, $subject, $body){
    // Create the Transport
    $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
        ->setUsername('ocholarosemary19@gmail.com')
        ->setPassword('hvhzqfiqlrigrdyg')
    ;

// Create the Mailer using your created Transport
    $mailer = new Swift_Mailer($transport);

// Create a message
    $message = (new Swift_Message($subject))
        ->setFrom(['ocholarosemary19@gmail.com' => 'AGM Campaign'])
        ->setTo([$receiver => $receiver])
        ->setBody($body)
    ;

    // Send the message
    return $mailer->send($message);
}

function sendSMS($msisdn, $message)
{
    //sms config
    $apiKey="MTo6Ojo1NTo6OjoxOjo6OlhRTTl1dTV2ZHF0dk5XSWhpUXlqdWd6cUFmTEVFTVV1NHU0cm9mRFNIT1V5Q18xbkRFRUYxXzhBTWVSRThoUTY=";
    $url="https://vas.wasiliana.co.ke/api/message/send/sms";
    $sender_id="LEGITIMATE";
    $postData = array(
        "message" => $message,
        "msisdn" => $msisdn,
        "sender_id" => $sender_id
    );
    $httpRequest = curl_init($url);
    curl_setopt($httpRequest, CURLOPT_NOBODY, true);
    curl_setopt($httpRequest, CURLOPT_POST, true);
    curl_setopt($httpRequest, CURLOPT_POSTFIELDS, json_encode($postData));
    curl_setopt($httpRequest, CURLOPT_TIMEOUT, 10);
    curl_setopt($httpRequest, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($httpRequest, CURLOPT_HTTPHEADER, array(
        'X-TOKEN: '.$apiKey,
        'Content-Type: application/json',
        'Content-Length: ' . strlen(json_encode($postData))));
    $postresponse = curl_exec($httpRequest);
    $httpStatusCode = curl_getinfo($httpRequest, CURLINFO_HTTP_CODE); //get status code
    curl_close($httpRequest);
    $response = array(
        'httpStatus' => $httpStatusCode,
        'response' => json_decode($postresponse)
    );
    return $response;
}

