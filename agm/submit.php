<?php
require 'agm.php';
require 'backend.php';

$response = array(
    'status'=>0,
    'message'=>'Form submission failed. Please try again',
);

if (isset($_POST['form'])){
    $role_id = $_POST['role_id'];

    if ($_POST['form'] == 'changePassword'){
        $email = mysqli_real_escape_string($con, $_POST['email']);
        $old_password = mysqli_real_escape_string($con, $_POST['old_password']);
        $password = mysqli_real_escape_string($con, $_POST['password']);
        $password1 = mysqli_real_escape_string($con, $_POST['password1']);

        if (!empty($email) && !empty($old_password) && !empty($password) && !empty($password1)){
            $pass = mysqli_num_rows(mysqli_query($con, "SELECT * from admins WHERE email = '$email' AND password = sha1('$old_password')"));

            if ($password !== $password1){
                $response['status'] = 401;
                $response['message'] = "Password does not match";
            }elseif (strlen($password) < 8){
                $response['status'] = 401;
                $response['message'] = "Password too short";
            }elseif ($pass == 1){
                $change = $con->query("UPDATE admins SET password = sha1('$password') WHERE email = '$email'");
                if ($change){
                    $response['status'] = 200;
                    $response['message'] = "Your password has been changed successfully";
                }else{
                    $response['status'] = 500;
                    $response['message'] = "Error occurred: ".mysqli_error($con);
                }
            }else{
                $response['status'] = 400;
                $response['message'] = "Wrong Email Address or Password";
            }
        }else{
            $response['status'] = 401;
            $response['message'] = "Please fill all the fields";
        }
    }else{
        if ($role_id == 100 || $role_id == 90 || $role_id == 50){
            switch ($_POST['form']){
                case "timeline":
                    if ($_POST['set'] == 'unset'){
                        $unset = $con->query("DELETE FROM timeline");
                        if ($unset){
                            $response['status'] = 200;
                            $response['message'] = "Timeline Unset Successful";
                        }else{
                            $response['status'] = 500;
                            $response['message'] = "Error occurred: ".mysqli_error($con);
                        }
                    }else{
                        if (isset($_POST['starttime']) || isset($_POST['endtime'])){
                            $starttime = mysqli_real_escape_string($con, $_POST['starttime']);
                            $endtime = mysqli_real_escape_string($con, $_POST['endtime']);

                            $no = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM timeline"));
                            $faq_no = $no['id'];

                            if (!empty($starttime) && !empty($endtime)){
                                if (strtotime($starttime) > strtotime($endtime)){
                                    $response['status'] = 500;
                                    $response['message'] = "End Time must be the latest";
                                }else{
                                    if ($_POST['role_id'] != 100){
                                        $response['status'] = 401;
                                        $response['message'] = "You dont have permission";

                                    }else{
                                        if (!empty($faq_no)){
                                            $faq = $con->query("UPDATE timeline SET starttime = '$starttime', endtime = '$endtime' WHERE id = '$faq_no'");
                                        }else{
                                            $faq = $con->query("INSERT INTO timeline (starttime, endtime) VALUES ('$starttime','$endtime')");
                                        }

                                        if ($faq){
                                            $response['status'] = 200;
                                            $response['message'] = "Timeline set Successful";
                                        }else{
                                            $response['status'] = 500;
                                            $response['message'] = "Error occurred: ".mysqli_error($con);
                                        }
                                    }

                                }
                            }else{
                                $response['status'] = 401;
                                $response['message'] = "Please fill all the fields";
                            }
                        }else{
                            $response['status'] = 400;
                            $response['message'] = "Fields not set";
                        }
                    }
                    break;
                case "agendas":
                    if (isset($_POST['name']) || isset($_POST['question']) || isset($_POST['type'])){
                        $name = mysqli_real_escape_string($con, $_POST['name']);
                        $question = mysqli_real_escape_string($con, $_POST['question']);
                        $type = mysqli_real_escape_string($con, $_POST['type']);

                        $no = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM agendas ORDER BY agenda_no DESC LIMIT 1"));
                        $agenda_no = $no['agenda_no'] + 1;

                        if (!empty($name) && !empty($question) && !empty($type)){

                            if ($_POST['type'] == 'Closed'){

                                if (!empty($_POST['answer'])){
                                    $query = "INSERT INTO agendas (agenda_no, name, question, type, created) VALUES ('$agenda_no' ,'$name', '$question', '$type', now())";
                                    $agenda = $con->query($query);

                                    $name = $_POST['name'];
                                    $fetch = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM agendas WHERE name = '$name' ORDER BY id DESC LIMIT 1"));
                                    $agenda_no = $fetch['agenda_no'];

                                    foreach ($_POST['answer'] as $ans){
                                        $fetch_no = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM answers WHERE agenda_no = '$agenda_no' ORDER BY id DESC LIMIT 1"));
                                        $answer_no = $fetch_no['answer_no'] + 1;

                                        $answer = mysqli_real_escape_string($con, $ans);

                                        $query1 = "INSERT INTO answers (agenda_no, answer_no, answer, created) VALUES ('$agenda_no', '$answer_no', '$answer', now())";
                                        $agenda1 = $con->query($query1);
                                    }

                                    if ($agenda){
                                        if ($agenda1){
                                            $response['status'] = 200;
                                            $response['message'] = "Agendas inserted Successfully";
                                        }else{
                                            $response['status'] = 500;
                                            $response['message'] = "Error occurred: ".mysqli_error($con);
                                        }
                                    }else{
                                        $response['status'] = 500;
                                        $response['message'] = "Error occurred: ".mysqli_error($con);
                                    }
                                }else{
                                    $response['status'] = 201;
                                    $response['message'] = "It is a closed question. Please provide answers";
                                }
                            }else{
                                $query = "INSERT INTO agendas (agenda_no, name, question, type, created) VALUES ('$agenda_no' ,'$name', '$question', '$type', now())";
                                $agenda = $con->query($query);

                                if ($agenda){
                                    $response['status'] = 200;
                                    $response['message'] = "Agendas inserted Successfully".$_POST['answer'];
                                }else{
                                    $response['status'] = 500;
                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                }
                            }
                        }else{
                            $response['status'] = 401;
                            $response['message'] = "Please fill all the fields";
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "member":
                    if (isset($_POST['MemberNo']) || isset($_POST['CDS_ClientID']) || isset($_POST['PaymentName']) || isset($_POST['ID_RegCert_No'])){
                        $phoneNo = formatMobileNumber($_POST['phoneNo']);
                        $shares = mysqli_real_escape_string($con, $_POST['shares']);
                        $MemberNo = mysqli_real_escape_string($con, $_POST['MemberNo']);
                        $CDS_ClientID = mysqli_real_escape_string($con, $_POST['CDS_ClientID']);
                        $PaymentName = mysqli_real_escape_string($con, $_POST['PaymentName']);
                        $ID_RegCert_No = mysqli_real_escape_string($con, $_POST['ID_RegCert_No']);
                        $full_name = mysqli_real_escape_string($con, $_POST['full_name']);

                        if (!empty($phoneNo) && !empty($MemberNo) && !empty($CDS_ClientID) && !empty($PaymentName) && !empty($ID_RegCert_No) && !empty($full_name)){
                            $query = mysqli_num_rows(mysqli_query($con, "SELECT * FROM users WHERE MemberNo = '$MemberNo' OR ID_RegCert_No = '$ID_RegCert_No'"));
                            if ($query > 0){
                                $response['status'] = 400;
                                $response['message'] = "Member with the details already exist";
                            }else{
                                $member = $con->query("INSERT INTO users (phoneNo, MemberNo, CDS_ClientID, full_name, PaymentName, ID_RegCert_No, shares, created) VALUES ('$phoneNo', '$MemberNo', '$CDS_ClientID', '$full_name', '$PaymentName', '$ID_RegCert_No', '$shares', now())");

                                if ($member){
                                    $response['status'] = 200;
                                    $response['message'] = "Member created Successfully";
                                }else{
                                    $response['status'] = 500;
                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                }
                            }
                        }else{
                            $response['status'] = 401;
                            $response['message'] = "Please fill all the fields";
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "register":
                    if ($_POST['role_id'] == 100){
                        if (isset($_POST['first_name']) || isset($_POST['last_name']) || isset($_POST['email']) || isset($_POST['password']) || isset($_POST['role'])){
                            $first_name = mysqli_real_escape_string($con, $_POST['first_name']);
                            $last_name = mysqli_real_escape_string($con, $_POST['last_name']);
                            $email = mysqli_real_escape_string($con, $_POST['email']);
                            $role_id = mysqli_real_escape_string($con, $_POST['role']);
                            $password = sha1($_POST['password']);

                            if (!empty($first_name) && !empty($last_name) && !empty($email) && !empty($password) && !empty($role_id)){

                                $query = mysqli_num_rows(mysqli_query($con, "SELECT * FROM admins WHERE email = '$email'"));
                                if ($query > 0){
                                    $response['status'] = 400;
                                    $response['message'] = "User with the email address already exist";
                                }else{
                                    $member = $con->query("INSERT INTO admins (first_name, last_name, email, password, role_id, created) VALUES ('$first_name', '$last_name', '$email', '$password', '$role_id', now())");

                                    if ($member){
                                        $response['status'] = 200;
                                        $response['message'] = "User created Successfully";
                                    }else{
                                        $response['status'] = 500;
                                        $response['message'] = "Error occurred: ".mysqli_error($con);
                                    }
                                }
                            }else{
                                $response['status'] = 401;
                                $response['message'] = "Please fill all the fields";
                            }
                        }else{
                            $response['status'] = 400;
                            $response['message'] = "Fields not set";
                        }
                    }else{
                        $response['status'] = 201;
                        $response['message'] = "You are Not authorised to create a new user";
                    }
                    break;
                case "shares":
                    if (isset($_POST['shares']) || isset($_POST['MemberNo'])){
                        $shares = mysqli_real_escape_string($con, $_POST['shares']);
                        $MemberNo = mysqli_real_escape_string($con, $_POST['MemberNo']);

                        if (!empty($shares) && !empty($MemberNo)){
                            if ($shares < 1){
                                $shares = $con->query("DELETE FROM users WHERE WHERE MemberNo = '$MemberNo'");
                                $vote = $con->query("DELETE FROM votes WHERE voter_MemberNo='$MemberNo'");
                                if ($shares){
                                    $response['status'] = 200;
                                    $response['message'] = "Shares update Successfully";
                                }else{
                                    $response['status'] = 500;
                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                }
                            }else{
                                $shares = $con->query("UPDATE users SET shares = '$shares' WHERE MemberNo = '$MemberNo'");
                                $vote = $con->query("UPDATE votes SET shares = '$shares' WHERE voter_MemberNo = '$MemberNo'");
                                if ($shares){
                                    $response['status'] = 200;
                                    $response['message'] = "Shares update Successfully";
                                }else{
                                    $response['status'] = 500;
                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                }
                            }

                        }else{
                            $response['status'] = 401;
                            $response['message'] = "Please fill all the fields";
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "faq":
                    if (isset($_POST['question']) || isset($_POST['answer'])){
                        $question = mysqli_real_escape_string($con, $_POST['question']);
                        $answer = mysqli_real_escape_string($con, $_POST['answer']);
                        $creator = $_POST['creator'];

                        $no = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM faq ORDER BY no DESC LIMIT 1"));
                        $faq_no = $no['no'] + 1;

                        if (!empty($question) && !empty($answer)){
                            $faq = $con->query("INSERT INTO faq (no, question, answer, creator, created) VALUES ('$faq_no','$question', '$answer', '$creator', now())");


                            if ($faq){
                                $response['status'] = 200;
                                $response['message'] = "FAQ added Successfully";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 401;
                            $response['message'] = "Please fill all the fields";
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "Delete":
                    $id = $_POST['id'];

                    if($id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM agendas WHERE id=".$id);
                        $totalrows = mysqli_num_rows($checkRecord);
                        $genda_del = mysqli_fetch_assoc($checkRecord);

                        if($totalrows > 0){
                            // Delete record
                            $delete = $con->query("DELETE FROM agendas WHERE id=".$id);
                            $delete_ans = $con->query("DELETE FROM answers WHERE agenda_no=".$genda_del['agenda_no']);
                            $delete_vote = $con->query("DELETE FROM votes WHERE agenda_id=".$id);
                            if ($delete){
                                if ($delete_ans){
                                    $response['status'] = 200;
                                    $response['message'] = "Agenda of ID: <code><b style='color: black;'>$id</b></code> Deleted Successful";
                                }else{
                                    $response['status'] = 500;
                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                }
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "DelMembers":
                    $id = $_POST['id'];

                    if($id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM users WHERE id=".$id);
                        $totalrows = mysqli_fetch_assoc($checkRecord);

                        if(!empty($totalrows['id'])){
                            // Delete record
                            $delete = $con->query("DELETE FROM users WHERE id=".$id);
                            $vote = $con->query("DELETE FROM votes WHERE voter_MemberNo=".$totalrows['MemberNo']);

                            if ($delete){
                                $response['status'] = 200;
                                $response['message'] = "Member of ID: <code><b style='color: black;'>$id</b></code> Deleted Successful";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "EditMembers":
                    if (isset($_POST['id'])){
                        $id = $_POST['id'];
                        $phoneNo = formatMobileNumber($_POST['phoneNo']);
                        $shares = mysqli_real_escape_string($con, $_POST['shares']);
                        $MemberNo = mysqli_real_escape_string($con, $_POST['MemberNo']);
                        $CDS_ClientID = mysqli_real_escape_string($con, $_POST['CDS_ClientID']);
                        $PaymentName = mysqli_real_escape_string($con, $_POST['PaymentName']);
                        $ID_RegCert_No = mysqli_real_escape_string($con, $_POST['ID_RegCert_No']);
                        $full_name = mysqli_real_escape_string($con, $_POST['full_name']);

                        $members = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM users WHERE id = '$id'"));

                        if (isset($_POST['popup'])){
                            if ($members){
                                $response['status'] = 200;
                                $response['message'] = '<div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="phoneNo">Phone Number</label>
                                      <input type="text" id="phoneNo" name="phoneNo" class="form-control form-control-alternative" value="'.$members['phoneNo'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="MemberNo">Membership No</label>
                                      <input type="number" id="MemberNo" name="MemberNo" class="form-control form-control-alternative" value="'.$members['MemberNo'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="full_name">Full Name</label>
                                      <input type="text" id="full_name" name="full_name" class="form-control form-control-alternative" value="'.$members['full_name'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="CDS_ClientID">CDS Client ID</label>
                                      <input type="text" id="CDS_ClientID" name="CDS_ClientID" class="form-control form-control-alternative" value="'.$members['CDS_ClientID'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="PaymentName">Payment Name</label>
                                      <input type="text" id="PaymentName" name="PaymentName" class="form-control form-control-alternative" value="'.$members['PaymentName'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="ID_RegCert_No">ID Number</label>
                                      <input type="number" id="ID_RegCert_No" name="ID_RegCert_No" class="form-control form-control-alternative" value="'.$members['ID_RegCert_No'].'">
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="shares">Shares</label>
                                      <input type="number" id="shares" name="shares" class="form-control form-control-alternative" value="'.$members['shares'].'">
                                  </div>
                              </div>
                              <input type="hidden" name="id" value="'.$id.'">';
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $edit = $con->query("UPDATE users SET phoneNo = '$phoneNo', full_name = '$full_name', MemberNo = '$MemberNo', CDS_ClientID = '$CDS_ClientID', PaymentName = '$PaymentName', ID_RegCert_No = '$ID_RegCert_No', shares = '$shares' WHERE id = '$id'");
                            if ($edit){
                                $response['status'] = 200;
                                $response['message'] = '<p class="alert alert-success">Member Edited successfully</p>';
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "DelProxy":
                    $id = $_POST['id'];

                    if($id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM proxy_users WHERE id=".$id);
                        $totalrows = mysqli_num_rows($checkRecord);

                        if($totalrows > 0){
                            // Delete record
                            $delete = $con->query("DELETE FROM proxy_users WHERE id=".$id);
                            if ($delete){
                                $response['status'] = 200;
                                $response['message'] = "Proxy User of ID: <code><b style='color: black;'>$id</b></code> Deleted Successful";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "DelDoc":
                    $id = $_POST['id'];

                    if($id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM downloads WHERE id=".$id);
                        $totalrows = mysqli_num_rows($checkRecord);

                        if($totalrows > 0){
                            // Delete record
                            $delete = $con->query("DELETE FROM downloads WHERE id=".$id);
                            if ($delete){
                                $response['status'] = 200;
                                $response['message'] = "Document of ID: <code><b style='color: black;'>$id</b></code> Deleted Successful";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "resetPassword":
                    // username and password sent from form
                    $reset_code = mysqli_real_escape_string($con, $_POST['reset_code']);
                    $password = mysqli_real_escape_string($con, $_POST['password']);
                    $confirm_password = mysqli_real_escape_string($con, $_POST['confirm_password']);
                    $email = mysqli_real_escape_string($con, $_POST['email']);

                    if (!empty($reset_code) && !empty($password) && !empty($confirm_password) && !empty($email)){
                        if ($password == $confirm_password){
                            if (strlen($password) > 7){
                                $sql = "SELECT * FROM reset_password WHERE email = '$email' and reset_code = '$reset_code'";
                                $result_login = mysqli_query($con, $sql);
                                $row_login = mysqli_fetch_array($result_login, MYSQLI_ASSOC);

                                $count = mysqli_num_rows($result_login);

                                if ($count == 1) {

                                    //change the password
                                    $sql = "UPDATE admins SET password = sha1('$password') WHERE email = '$email'";
                                    $result_reset = mysqli_query($con, $sql);

                                    if ($result_reset){
                                        $sql1 = "DELETE FROM reset_password WHERE email = '$email'";
                                        $clear = mysqli_query($con, $sql1);

                                        $response['status'] = 200;
                                        $response['message'] = "Success.";

                                    }
                                } else {
                                    $response['status'] = 500;
                                    $response['message'] = "Invalid Code. Please recheck and try again.";

                                }
                            } else {
                                $response['status'] = 500;
                                $response['message'] = "Password too short.";

                            }
                        }else{
                            $response['status'] = 400;
                            $response['message'] = "Password does not match. Please try again";
                        }
                    }else{
                        $response['status'] = 404;
                        $response['message'] = "Please fill in all the fields";
                    }
                    break;
                case "doc":
                    // Get the submitted form data
                    $name = mysqli_real_escape_string($con, $_POST['name']);
                    $uploader = mysqli_real_escape_string($con, $_POST['uploader']);
                    $type = mysqli_real_escape_string($con, $_POST['type']);
                    $text = mysqli_real_escape_string($con, $_POST['text']);

                    //save file to the folder required
                    $uploadDir = '../uploads/';

                    // Check whether submitted data is not empty
                    if(!empty($name)){
                        $no = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM downloads ORDER BY no DESC LIMIT 1"));
                        $file_no = $no['no'] + 1;

                        if ($type == 'text'){
                            // Insert form data in the database
                            $insert = $con->query("INSERT INTO downloads (no, type, file_name,name,uploader,created) VALUES ('".$file_no."', '".$type."', '".$text."','".$name."','".$uploader."', now())");

                            if($insert){
                                $response['status'] = 200;
                                $response['message'] = 'Form data submitted successfully!';
                            }else{
                                $response['status'] = 500;
                                $response['message'] = 'Submission failed. Error: '.mysqli_error($con);
                            }
                        }else{
                            // Count total files
                            $countfiles = count($_FILES['upload']['name']);


                            // Upload file
                            $uploadedFile = '';
                            if($countfiles > 0){
                                // Looping all files
                                for($i=0;$i<$countfiles;$i++){


                                    // File path config
                                    $fileName = $_FILES['upload']['name'][$i];
                                    $targetFilePath = $uploadDir . $fileName;
                                    $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

                                    // Check if image file is a actual image or fake image
                                    $check = filesize($_FILES["upload"]["tmp_name"]);

                                    if($check !== true) {
                                        $response['status'] = 201;
                                        $response['message'] = "Got the file - " . $check["mime"] . ".";
                                    } else {
                                        $response['status'] = 404;
                                        $response['message'] = "No file uploaded";
                                    }

                                    // Check if file already exists
                                    if (file_exists($targetFilePath)) {
                                        $response['status'] = 400;
                                        $response['message'] = "Sorry, file already exists.";
                                    }

                                    // Check file size
                                    if ($_FILES["fileToUpload"]["size"] > 10000000) {
                                        $response['status'] = 400;
                                        $response['message'] = "Sorry, your file is too large.";

                                    }

                                    // Allow certain file formats
                                    $allowTypes = array('pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg', 'csv');
                                    if(in_array($fileType, $allowTypes)){
                                        if ($response['status'] == 201){
                                            // Upload file to the server
                                            if(move_uploaded_file($_FILES["upload"]["tmp_name"][$i], $targetFilePath)){
                                                $uploadedFile = $fileName;

                                                // Insert form data in the database
                                                $insert = $con->query("INSERT INTO downloads (no, type, file_name,name,uploader,created) VALUES ('".$file_no."', '".$type."', '".$uploadedFile."','".$name."','".$uploader."', now())");

                                                if($insert){
                                                    $response['status'] = 200;
                                                    $response['message'] = 'Form data submitted successfully!';
                                                }else{
                                                    $response['status'] = 500;
                                                    $response['message'] = 'Submission failed. Error: '.mysqli_error($con);
                                                }

                                            }else{
                                                $response['status'] = 404;
                                                $response['message'] = 'Sorry, there was an error uploading your file.';
                                            }
                                        }
                                    }else{
                                        $response['status'] = 404;
                                        $response['message'] = 'Sorry, only PDF, DOC, CSV, JPG, JPEG, & PNG files are allowed to upload.';
                                    }

                                }

                            }else{
                                $response['message'] = 'Empty File.';
                            }
                        }

                    }else{
                        $response['message'] = 'Please fill all the mandatory fields (name and email).';
                    }
                    break;
                case "DelAdmin":
                    $users_id = $_POST['users_id'];

                    if($users_id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM admins WHERE id=".$users_id);
                        $totalrows = mysqli_fetch_assoc($checkRecord);

                        if(!empty($totalrows['id'])){
                            // Delete record
                            $delete = $con->query("DELETE FROM admins WHERE id=".$users_id);
                            if ($delete){
                                $response['status'] = 200;
                                $response['message'] = "User of ID: <code><b style='color: black;'>$users_id</b></code> Deleted Successful";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "import":
                    $target_dir = "../uploads/import/";
                    $fileName = $_FILES['importfile']['name'];
                    $target_file = $target_dir . $fileName;
                    $allowTypes = array('csv');

                    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
                    if (in_array($imageFileType, $allowTypes)){

                        // Allow certain file formats
                        $allowTypes = array('csv');
                        if(in_array($imageFileType, $allowTypes)){
                            if (move_uploaded_file($_FILES["importfile"]["tmp_name"], $target_file)) {

                                // Checking file exists or not
                                $fileexists = 0;
                                if (file_exists($target_file)) {
                                    $fileexists = 1;
                                }
                                if ($fileexists == 1 ) {

                                    // Reading file
                                    $file = fopen($target_file,"r");
                                    $i = 0;

                                    $importData_arr = array();

                                    while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
                                        $num = count($data);

                                        for ($c=0; $c < $num; $c++) {
                                            $importData_arr[$i][] = $data[$c];
                                        }
                                        $i++;
                                    }
                                    fclose($file);

                                    $skip = 0;
                                    // insert import data
                                    foreach($importData_arr as $data){
                                        if($skip != 0){
                                            $phoneNo = formatMobileNumber($data[0]);
                                            $MemberNo = mysqli_real_escape_string($con, $data[1]);
                                            $CDS_ClientID = mysqli_real_escape_string($con, $data[2]);
                                            $full_name = mysqli_real_escape_string($con, $data[3]);
                                            $ID_RegCert_No = mysqli_real_escape_string($con, $data[4]);
                                            $shares = mysqli_real_escape_string($con, $data[5]);

                                            // Checking duplicate entry
                                            $sql = "select count(*) as allcount from users where MemberNo='$MemberNo' and CDS_ClientID='$CDS_ClientID' and ID_RegCert_No='$ID_RegCert_No' ";

                                            $retrieve_data = mysqli_query($con,$sql);
                                            $row = mysqli_fetch_array($retrieve_data);
                                            $count = $row['allcount'];

                                            if($count == 0){
                                                // Insert record
                                                $insert_query = "insert into users (phoneNo,MemberNo,CDS_ClientID,full_name,ID_RegCert_No, shares, created) values ('$phoneNo','$MemberNo','$CDS_ClientID', '$full_name', '$ID_RegCert_No', '$shares', now())";
                                                $insert = mysqli_query($con,$insert_query);
                                                if ($insert){
                                                    $response['status'] = 200;
                                                    $response['message'] = "File uploaded successfully ";
                                                }else{
                                                    $response['message'] = "Error: ".mysqli_error($con)."Query:  ".$insert_query;
                                                }
                                            }
                                        }
                                        $skip ++;
                                    }

                                    $newtargetfile = $target_file;
                                    if (file_exists($newtargetfile)) {
                                        unlink($newtargetfile);
                                    }


                                }

                            }else{
                                $response['message'] = "File not uploaded";
                            }
                        }else{
                            $response['status'] = 500;
                            $response['message'] = "Only CSV files accepted";
                        }

                    }else{

                        $response['status'] = 500;
                        $response['message'] = "Only csv file required";
                    }
                    break;
                case "DelQuiz":
                    $id = $_POST['id'];

                    if($id > 0){

                        // Check record exists
                        $checkRecord = mysqli_query($con,"SELECT * FROM quiz WHERE id=".$id);
                        $totalrows = mysqli_fetch_assoc($checkRecord);

                        if(!empty($totalrows['id'])){
                            // Delete record
                            $delete = $con->query("DELETE FROM quiz WHERE id=".$id);
                            if ($delete){
                                $response['status'] = 200;
                                $response['message'] = "Question of ID: <code><b style='color: black;'>$id</b></code> Deleted Successful";
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 404;
                            $response['message'] = "No data to delete";
                        }
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error. No data passed";
                    }
                    break;
                case "AnswerQuiz":
                    if (isset($_POST['id'])){
                        $id = $_POST['id'];
                        $answer = mysqli_real_escape_string($con, $_POST['answer']);

                        $quiz = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM quiz WHERE id = '$id'"));

                        if (isset($_POST['popup'])){
                            if ($members){
                                $response['status'] = 200;
                                $response['message'] = '<div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="quiz">Question</label>
                                      <input type="number" id="quiz" name="quiz" class="form-control form-control-alternative" value="'.$quiz['quiz'].'" readonly>
                                  </div>
                              </div>
                              <div class="col-12">
                                  <div class="form-group">
                                      <label class="form-control-label" for="answer">Answer</label>
                                      <input type="text" id="answer" name="answer" class="form-control form-control-alternative">
                                  </div>
                              </div>
                              <input type="hidden" name="id" value="'.$id.'">';
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $edit = $con->query("UPDATE quiz SET answer = '$answer' WHERE id = '$id'");
                            if ($edit){
                                sendSMS($quiz['user'], $answer);
                                $response['status'] = 200;
                                $response['message'] = '<p class="alert alert-success">Answer submitted successfully</p>';
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "sms":
                    if (isset($_POST['receivers']) || isset($_POST['message'])){
                        $message = mysqli_real_escape_string($con, $_POST['message']);
                        $receivers = mysqli_real_escape_string($con, $_POST['receivers']);

                        if (!empty($message) && !empty($receivers)){
                            $insert = mysqli_query($con, "insert into sms (message, total, created) values ('$message', 0, now())");
                            if ($insert){
                                if ($receivers == 'membersProxy'){
                                    $query1 = mysqli_query($con, "SELECT * FROM users");
                                    $query2 = mysqli_query($con, "SELECT * FROM proxy_users");

                                    $sms1 = mysqli_fetch_assoc($query1);
                                    $sms2 = mysqli_fetch_assoc($query2);

                                    do{
                                        $send1 = sendSMS($sms1['phoneNo'], $message);

                                        if ($send1){
                                            $total = mysqli_fetch_assoc(mysqli_query($con, "select * from sms ORDER BY id DESC LIMIT 1"));
                                            $tt = $total['total'] + 1;
                                            $id = $total['id'];

                                            $update = mysqli_query($con, "update sms set total = '$tt' where id = '$id'");
                                            if ($update){
                                                $response['status'] = 200;
                                                $response['message'] = 'SMS send Successfully';
                                            }else{
                                                $response['status'] = 400;
                                                $response['message'] = "Error occurred: ".mysqli_error($con);
                                            }
                                        }
                                    }while($sms1 = mysqli_fetch_assoc($query1));

                                    do{
                                        $send = sendSMS($sms2['phoneNo'], $message);

                                        if ($send){
                                            $total = mysqli_fetch_assoc(mysqli_query($con, "select * from sms ORDER BY id DESC LIMIT 1"));
                                            $tt = $total['total'] + 1;
                                            $id = $total['id'];

                                            $update = mysqli_query($con, "update sms set total = '$tt' where id = '$id'");
                                            if ($update){
                                                $response['status'] = 200;
                                                $response['message'] = 'SMS send Successfully';
                                            }else{
                                                $response['status'] = 400;
                                                $response['message'] = "Error occurred: ".mysqli_error($con);
                                            }
                                        }
                                    }while($sms2 = mysqli_fetch_assoc($query2));

                                }else{
                                    if ($receivers == 'members'){

                                        $query = mysqli_query($con, "SELECT * FROM users");
                                    }elseif ($receivers == 'attenders'){

                                        $query = mysqli_query($con, "SELECT * FROM attendance");
                                    }elseif ($receivers == 'voters'){

                                        $query = mysqli_query($con, "SELECT * FROM votes");
                                    }elseif ($receivers == 'proxy'){

                                        $query = mysqli_query($con, "SELECT * FROM proxy_users");
                                    }

                                    $sms = mysqli_fetch_assoc($query);

                                    if ($sms){
                                        do{
                                            $send = sendSMS($sms['phoneNo'], $message);

                                            if ($send){
                                                $total = mysqli_fetch_assoc(mysqli_query($con, "select * from sms ORDER BY id DESC LIMIT 1"));
                                                $tt = $total['total'] + 1;
                                                $id = $total['id'];

                                                $update = mysqli_query($con, "update sms set total = '$tt' where id = '$id'");
                                                if ($update){
                                                    $response['status'] = 200;
                                                    $response['message'] = 'SMS send Successfully';
                                                }else{
                                                    $response['status'] = 400;
                                                    $response['message'] = "Error occurred: ".mysqli_error($con);
                                                }
                                            }
                                        }while($sms = mysqli_fetch_assoc($query));
                                    }else{
                                        $response['status'] = 500;
                                        $response['message'] = "Error occurred: ".mysqli_error($con);
                                    }
                                }
                            }else{
                                $response['status'] = 500;
                                $response['message'] = "Error occurred: ".mysqli_error($con);
                            }
                        }else{
                            $response['status'] = 400;
                            $response['message'] = "Pleases fill all the fields";
                        }
                    }else{
                        $response['status'] = 400;
                        $response['message'] = "Fields not set";
                    }
                    break;
                case "resetAttendance":
                    // Delete record
                    $delete = $con->query("DELETE FROM attendance");
                    $del = $con->query("DELETE FROM votes");
                    if ($delete && $del){
                        $response['status'] = 200;
                        $response['message'] = "Voting reset successfully";
                    }else{
                        $response['status'] = 500;
                        $response['message'] = "Error occurred: ".mysqli_error($con);
                    }
                    break;
                default:
                    $response['status'] = 500;
                    $response['message'] = "Error Default";
            }
        }else{
            $response['status'] = 500;
            $response['message'] = "You have no Permission";
        }
    }

}else{
    $response['status'] = 404;
    $response['message'] = "No form submitted";
}

echo json_encode($response);
