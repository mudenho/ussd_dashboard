<?php
include 'agm.php';

// Initialize the session
session_start();

//minutes or more.
$expireAfter = 30;

//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){

    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];

    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;

    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        session_unset();
        session_destroy();
    }

}

//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ../login");
    exit;
}

//get the information of the user logged in
$sql_ses = sprintf("SELECT * FROM admins WHERE email = '".$_SESSION["email"]."'");
$res = mysqli_query($con, $sql_ses) or die(mysqli_error($con));
$rw = mysqli_fetch_array($res);

$first_name = $rw['first_name'];
$last_name = $rw['last_name'];
$email = $rw['email'];
$role_id = $rw['role_id'];
