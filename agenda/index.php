<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | AGENDA</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../members">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../proxy">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../attendance">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../advanced">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Agenda</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Agenda</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row" style="padding-bottom: 20px">
        <div class="col-xl-5 order-xl-2">
            <div class="table-responsive">
                <div id="mytable">
                    <?php if (count($agenda['id']) > 0){ ?>
                    <table class="table align-items-center table-dark">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col" class="sort" data-sort="id">Agenda No</th>
                            <th scope="col" class="sort" data-sort="name">Agenda Name</th>
                            <th scope="col" class="sort" data-sort="question">Question</th>
                            <th scope="col" class="sort" data-sort="type">Type</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody class="list">
                        <?php do{ ?>
                            <tr>
                                <td class="id"><?php echo $agenda['agenda_no']; ?></td>
                                <td class="name"><?php echo $agenda['name']; ?></td>
                                <?php if ($agenda['type'] == 'Open'){ ?>
                                <td class="question"><?php echo $agenda['question']; ?></td>
                                <?php } else { ?>
                                <td class="question">
                                    <div class="accordion" id="accordionExample">
                                        <span class="collapsed" data-toggle="collapse" data-target="#<?php echo $agenda['name']; ?>" aria-expanded="true" aria-controls="collapseOne">
                                            <?php echo $agenda['question']; ?>
                                        </span>

                                        <div id="<?php echo $agenda['name']; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <ol>
                                                <?php
                                                $agenda_no = $agenda['agenda_no'];
                                                $query5 = mysqli_query($con, "SELECT * FROM answers WHERE agenda_no = '$agenda_no'");
                                                $quiz = mysqli_fetch_assoc($query5);
                                                do{ ?>
                                                    <li><?php echo $quiz['answer']; ?></li>
                                                <?php } while ($quiz = mysqli_fetch_assoc($query5)); ?>
                                            </ol>
                                        </div>
                                    </div>
                                </td>
                                <?php } ?>
                                <td class="type"><?php echo $agenda['type']; ?></td>
                                <td class="text-right">
                                    <form class="del" enctype="multipart/form-data">
                                        <input type="hidden" name="form" value="Delete">
                                        <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                        <input type="hidden" name="id" value="<?php echo $agenda['id']; ?>">
                                        <input type="submit" class='btn btn-sm btn-outline-danger' value="Delete">
                                    </form>
                                </td>
                            </tr>
                        <?php } while ($agenda = mysqli_fetch_assoc($query1)); ?>
                        </tbody>
                    </table>
                    <?php } ?>
                </div>
            </div>
        </div>
          <?php if ($role_id == 100 || $role_id == 90 || $role_id == 50){ ?>
        <div class="col-xl-7 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                  <div class="col-12">
                      <!--Message Response-->
                      <div class="message"></div>
                  </div>
                <div class="col-8">
                  <h3 class="mb-0">Create Agendas </h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form class="agenda" enctype="multipart/form-data">
                <div class="pl-lg-4">
                  <div class="row p-4 bg-secondary">
                      <div class="col-lg-12">
                          <div class="form-group">
                              <label class="form-control-label" for="name">Name of the Agenda</label>
                              <input type="text" id="name" name="name" class="form-control form-control-alternative" placeholder="e.g. Agenda 1" autocomplete="off">
                          </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="form-group">
                              <label class="form-control-label" for="question">Question</label>
                              <input type="text" id="question" name="question" class="form-control form-control-alternative" autocomplete="off" placeholder="e.g. Who do you vote for as the next CEO">
                          </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="form-group">
                              <label class="form-control-label" for="question">Type of Question</label>
                              <select class="form-control form-control-alternative" name="type">
                                  <option value="">Select Type</option>
                                  <option value="Open">Open Ended</option>
                                  <option value="Closed">Close Ended</option>
                              </select>
                          </div>
                      </div>

                      <div class="row AddInput" id="new_chq">
                          <div class="col-6"><button class="btn btn-primary btn-submit" id="addInput">Add Answer</button></div>
                          <div class="col-6"><button class="btn btn-primary btn-submit" id="remove">Remove Answer</button></div>
                          <input type="hidden" value="1" id="total_chq">
                      </div>
                  </div>
                    <div class="row">
                        <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                        <input type="hidden" name="form" value="agendas">
                        <input type="submit" class="btn btn-primary btn-submit">
                    </div>

                </div>
              </form>
            </div>
          </div>
        </div>
          <?php } ?>
      </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>


  <!--Form Submission-->
<script>
    $(document).ready(function (e) {
        $(".agenda").on('submit', function (e) {
            e.preventDefault();
            $(".message").html("<p class='alert alert-info'>Wait... Submitting data...</p>");
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".btn-submit").attr("disabled", "disabled");
                    $(".agenda").css("opacity", ".5");
                },
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        $('.agenda')[0].reset();
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                    $('.agenda').css("opacity","");
                    $(".btn-submit").removeAttr("disabled");
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('fast');
            }, 30000);
        });

        // Delete Button
        $(".del").on('submit', function (e) {
            e.preventDefault();
            var el = this;
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        // Remove row from HTML Table
                        $(el).closest('tr').css('background','tomato');
                        $(el).closest('tr').fadeOut(800,function(){
                            $(this).remove();
                        });
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('fast');
            }, 3000);
        })

        //Refresh table
        setInterval(function () {
            $( "#mytable" ).load( "index.php #mytable" );
        }, 5000);

    });

    //Hidden div
    $(document).ready(function(){
        $("select").change(function(){
            $( "select option:selected").each(function(){
                if($(this).attr("value")=="Closed"){
                    $(".AddInput").show();
                }else {
                    $(".AddInput").hide();
                }
            });
        }).change();
    });

    //Add input dynamically
    $('#addInput').on('click', add);
    $('#remove').on('click', remove);

    function add() {
        var new_chq_no = parseInt($('#total_chq').val()) + 1;
        var new_input = "<div class='col-lg-10'><div class='form-group'><input type='text' name='answer[]' id='new_" + new_chq_no + "' class='form-control form-control-alternative' placeholder='John Doe' autocomplete='off' required></div></div>";

        $('#new_chq').append(new_input);

        $('#total_chq').val(new_chq_no);
    }

    function remove() {
        var last_chq_no = $('#total_chq').val();

        if (last_chq_no > 1) {
            $('#new_' + last_chq_no).remove();
            $('#total_chq').val(last_chq_no - 1);
        }
    }

</script>
</body>

</html>