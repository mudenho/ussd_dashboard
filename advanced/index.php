<?php
require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | ADVANCED</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../agenda">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../members">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../proxy">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../attendance">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Advanced</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Advanced</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row align-items-center">
            <div class="col-xl-7">
                <!--Message Response-->
                <div class="message"></div>
            </div>
        </div>
      <div class="row">
          <?php if ($role_id == 100 ){ ?>
              <div class="col-xl-7">
                  <div class="card">
                      <div class="card-header">
                          <div class="row align-items-center">
                              <div class="col-8">
                                  <h3 class="mb-0">Create System Users</h3>
                              </div>
                          </div>
                      </div>
                      <div class="card-body">
                          <form class="advanced" enctype="multipart/form-data" id="advanced">
                              <div class="pl-lg-4">
                                  <div class="row p-4 bg-secondary">
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="first_name">First Name</label>
                                              <input type="text" id="first_name" name="first_name" class="form-control form-control-alternative" placeholder="e.g. John" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="last_name">Last Name</label>
                                              <input type="text" id="last_name" name="last_name" class="form-control form-control-alternative" placeholder="e.g. Doe" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="email">Email Address</label>
                                              <input type="email" id="email" name="email" class="form-control form-control-alternative" placeholder="e.g. smith@example.com" autocomplete="off">
                                          </div>
                                      </div>
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="role_id">Permission</label>
                                              <select class="form-control form-control-alternative" name="role">
                                                  <option value="">Select Permission</option>
                                                  <?php

                                                  $query3 = mysqli_query($con, "SELECT * FROM roles ORDER BY role_id DESC");
                                                  $role = mysqli_fetch_assoc($query3);
                                                  do{ ?>
                                                      <option value="<?php echo $role['role_id']; ?>"><?php echo $role['role_name']; ?></option>
                                                  <?php } while ($role = mysqli_fetch_assoc($query3)); ?>
                                              </select>
                                          </div>
                                      </div>
                                      <div class="col-lg-12">
                                          <div class="form-group">
                                              <label class="form-control-label" for="password">Password</label>
                                              <input type="password" id="password" name="password" class="form-control form-control-alternative" placeholder="e.g. 123$7grmxr">
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row">
                                      <input type="hidden" name="role_id" value="<?php echo $rw['role_id']; ?>">
                                      <input type="hidden" name="form" value="register">
                                      <input type="submit" class="btn btn-primary btn-submit">
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
              <div class="col-xl-5">
                  <div class="col-12" style="padding-bottom: 20px">
                      <form class="advanced" enctype="multipart/form-data">
                          <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                          <input type="hidden" name="form" value="resetAttendance">
                          <input type="submit" class="btn btn-warning btn-lg btn-block btn-submit" value="RESET VOTING">
                      </form>
                  </div>
                  <div class="col-12">
                      <div class="card">
                          <div class="card-header border-0">
                              <div class="row align-items-center">
                                  <div class="col">
                                      <h3 class="mb-0">Voting Timeline</h3>
                                  </div>
                              </div>
                          </div>
                          <div class="card-body">
                              <div class="card-header border-0">
                                  <form enctype="multipart/form-data" class="advanced">
                                      <div class="row align-items-center">
                                          <div class="input-daterange datepicker row align-items-center">
                                              <div class="col-12">
                                                  <div class="row">
                                                      <div class="col-md-3">
                                                          <label>Start Time</label>
                                                      </div>
                                                      <div class="col-md-9">
                                                          <div class="form-group">
                                                              <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                      <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                                  </div>
                                                                  <input name="starttime" type="datetime-local" class="form-control" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-12">
                                                  <div class="row">
                                                      <div class="col-md-3">
                                                          <label>End Time</label>
                                                      </div>
                                                      <div class="col-md-9">
                                                          <div class="form-group">
                                                              <div class="input-group">
                                                                  <div class="input-group-prepend">
                                                                      <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                                  </div>
                                                                  <input name="endtime" type="datetime-local" class="form-control" />
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class=" float-left">
                                          <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                          <input type="hidden" name="form" value="timeline">
                                          <input type="submit" class="btn btn-success btn-submit" value="Set">
                                      </div>
                                  </form>
                                  <form enctype="multipart/form-data" class="advanced">
                                      <div class="float-right">
                                          <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                          <input type="hidden" name="form" value="timeline">
                                          <input type="hidden" name="set" value="unset">
                                          <input type="submit" class="btn btn-danger btn-submit" value="Unset">
                                      </div>
                                  </form>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          <?php } ?>
      </div>
        <div class="row">
            <div class="col-xl-7">
                <div class="card">
                    <div class="card-header">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">Change Password</h3>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form class="advanced" enctype="multipart/form-data" id="change">
                            <div class="pl-lg-4">
                                <div class="row p-4 bg-secondary">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="email">Email Address</label>
                                            <input type="email" id="email" name="email" class="form-control form-control-alternative" placeholder="e.g. smith@example.com" autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="old_password">Old Password</label>
                                            <input type="password" id="old_password" name="old_password" class="form-control form-control-alternative" placeholder="e.g. 123$7grmxr">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="password">New Password</label>
                                            <input type="password" id="password" name="password" class="form-control form-control-alternative" placeholder="e.g. 123$7grmxr">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-label" for="password1">Confirm Password Password</label>
                                            <input type="password" id="password1" name="password1" class="form-control form-control-alternative" placeholder="e.g. 123$7grmxr">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <input type="hidden" name="role_id" value="<?php echo $rw['role_id']; ?>">
                                    <input type="hidden" name="form" value="changePassword">
                                    <input type="submit" class="btn btn-primary btn-submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-xl-5">
                <div class="table-responsive">
                    <div>
                        <table class="table align-items-center table-dark" id="mytable">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="id">ID</th>
                                <th scope="col" class="sort" data-sort="name">Full Name</th>
                                <th scope="col" class="sort" data-sort="email">Email Address</th>
                                <th scope="col" class="sort" data-sort="email">Permission</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            <?php
                            do{
                                $role_id = $users['role_id'];
                                $roles = mysqli_fetch_assoc(mysqli_query($con, "SELECT * from roles WHERE role_id = '$role_id'"));
                                ?>
                                <tr>
                                    <td class="id"><?php echo $users['id']; ?></td>
                                    <td class="name"><?php echo $users['last_name'], " ".$users['first_name']; ?></td>
                                    <td class="question"><?php echo $users['email']; ?></td>
                                    <td class="question"><?php echo $roles['role_name']; ?></td>
                                    <td class="text-right">
                                        <form class="del" enctype="multipart/form-data">
                                            <input type="hidden" name="form" value="DelAdmin">
                                            <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                            <input type="hidden" name="users_id" value="<?php echo $users['id']; ?>">
                                            <input type="submit" class='btn btn-sm btn-outline-danger' value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            <?php } while ($users = mysqli_fetch_assoc($query4)); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>


  <!--Form Submission-->
<script>
    $(document).ready(function (e) {
        $(".advanced").on('submit', function (e) {
            e.preventDefault();
            $(".message").html("<p class='alert alert-info'>Wait... Submitting data...</p>");
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".btn-submit").attr("disabled", "disabled");
                    $(".advanced").css("opacity", ".5");
                },
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        $('#change')[0].reset();
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                        setTimeout(function() {
                            $('.message').fadeOut('fast');
                        }, 30000);
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                    $('.advanced').css("opacity","");
                    $(".btn-submit").removeAttr("disabled");
                }
            });
        })
        // Delete Button
        $(".del").on('submit', function (e) {
            e.preventDefault();
            var el = this;
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        // Remove row from HTML Table
                        $(el).closest('tr').css('background','tomato');
                        $(el).closest('tr').fadeOut(800,function(){
                            $(this).remove();
                        });
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');

                        setTimeout(function() {
                            $('.message').fadeOut('fast');
                        }, 3000);
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                }
            });
        })

        //Refresh table
        setInterval(function () {
            $( "#mytable" ).load( "index.php #mytable" );
        }, 6000);

    })
</script>
</body>

</html>