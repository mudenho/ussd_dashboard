<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | DOCUMENTS</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="../">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../agenda">
                <i class="ni ni-bullet-list-67 text-orange"></i>
                <span class="nav-link-text">Agendas</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" href="#">
                <i class="ni ni-cloud-download-95 text-primary"></i>
                <span class="nav-link-text">Documents</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../members">
                <i class="ni ni-user-run text-yellow"></i>
                <span class="nav-link-text">Members</span>
              </a>
            </li>
              <li class="nav-item">
                  <a class="nav-link" href="../database">
                      <i class="ni ni-bullet-list-67 text-yellow"></i>
                      <span class="nav-link-text">Members Database</span>
                  </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="../proxy">
                <i class="ni ni-watch-time text-default"></i>
                <span class="nav-link-text">Proxy Users</span>
              </a>
            </li>
              <li class="nav-item">
                  <a class="nav-link" href="../attendance">
                      <i class="ni ni-archive-2 text-default"></i>
                      <span class="nav-link-text">Attendance</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../voting">
                      <i class="ni ni-collection text-default"></i>
                      <span class="nav-link-text">Voting</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../faq">
                      <i class="ni ni-book-bookmark text-default"></i>
                      <span class="nav-link-text">FAQs</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../sms">
                      <i class="ni ni-send text-primary"></i>
                      <span class="nav-link-text">Send SMS</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../advanced">
                      <i class="ni ni-single-02 text-default"></i>
                      <span class="nav-link-text">Advanced</span>
                  </a>
              </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Documents</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Documents</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row" style="padding-bottom: 20px">
        <div class="col-xl-6 order-xl-2">
            <div class="table-responsive">
                <div id="mytable">
                    <?php if (count($agenda['id']) > 0){ ?>
                        <table class="table align-items-center table-dark">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort" data-sort="id">ID</th>
                                <th scope="col" class="sort" data-sort="name">Document Name</th>
                                <th scope="col" class="sort" data-sort="question">Resource Content</th>
                                <th scope="col" class="sort" data-sort="type">Uploader</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody class="list">
                            <?php do{ ?>
                                <tr>
                                    <td class="id"><?php echo $doc['id']; ?></td>
                                    <td class="name"><?php echo $doc['name']; ?></td>
                                    <td class="question"><?php echo $doc['file_name']; ?></td>
                                    <td class="type"><?php echo $doc['uploader']; ?></td>
                                    <td class="text-right">
                                        <form class="del" enctype="multipart/form-data">
                                            <input type="hidden" name="form" value="DelDoc">
                                            <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                            <input type="hidden" name="id" value="<?php echo $doc['id']; ?>">
                                            <input type="submit" class='btn btn-sm btn-outline-danger' value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            <?php } while ($doc = mysqli_fetch_assoc($query6)); ?>
                            </tbody>
                        </table>
                    <?php } ?>
                </div>
            </div>
        </div>
          <?php if ($role_id == 100 || $role_id == 90 || $role_id == 50){ ?>
          <div class="col-xl-6 order-xl-1">
          <div class="card">
            <div class="card-header">
              <div class="row align-items-center">
                  <div class="col-12">
                      <!--Message Response-->
                      <div class="statusMsg"></div>
                  </div>
                <div class="col-8">
                  <h3 class="mb-0">Upload Resources </h3>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form id="docs" enctype="multipart/form-data">
                <div class="pl-lg-4">
                  <div class="row p-4 bg-secondary">
                      <div class="col-lg-12">
                          <div class="form-group">
                              <label class="form-control-label" for="question">Resource Type</label>
                              <select class="form-control form-control-alternative" name="type">
                                  <option value="document">Document</option>
                                  <option value="text">Text</option>
                              </select>
                          </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="form-group">
                              <label class="form-control-label" for="name">Name</label>
                              <input type="text" id="name" name="name" class="form-control form-control-alternative" placeholder="e.g. Minutes" autocomplete="off">
                          </div>
                      </div>
                      <div class="col-lg-12 document">
                          <div class="custom-file">
                              <input type="file" class="custom-file-input" id="upload" name="upload[]">
                              <label class="custom-file-label" for="customFileLang">Select file</label>
                          </div>
                      </div>
                      <div class="col-lg-12 text">
                          <div class="form-group">
                              <label class="form-control-label" for="text">Text</label>
                              <textarea name="text" cols="5" class="form-control form-control-alternative" autocomplete="off"></textarea>
                          </div>
                      </div>
                  </div>
                    <div class="row">
                        <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                        <input type="hidden" name="uploader" value="<?php echo $last_name," ".$first_name; ?>">
                        <input type="hidden" name="form" value="doc">
                        <input type="submit" class="btn btn-primary btn-submit">
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
          <?php } ?>
      </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>


  <!--Form Submission-->
<script>
    $(document).ready(function (e) {
        $("#docs").submit(function (event) {
            var data = new FormData();

            event.preventDefault();

            $.each($("#upload")[0].files, function (key, file){
                data.append(key, file);
            });

            $.each($('#docs').serializeArray(), function (i, obj) {
                data.append(obj.name, obj.value)
            });

            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $('.submitBtn').attr("disabled","disabled");
                    $('#docs').css("opacity",".5");
                },
                success: function(response){ //console.log(response);
                    $('.statusMsg').html('');
                    if(response.status == 200){
                        $('#docs')[0].reset();
                        $('.statusMsg').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.statusMsg').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                    $('#docs').css("opacity","");
                    $(".submitBtn").removeAttr("disabled");
                }
            });
            setTimeout(function() {
                $('.statusMsg').fadeOut('fast');
            }, 3000);
        });

        // Delete Button
        $(".del").on('submit', function (e) {
            e.preventDefault();
            var el = this;
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        // Remove row from HTML Table
                        $(el).closest('tr').css('background','tomato');
                        $(el).closest('tr').fadeOut(800,function(){
                            $(this).remove();
                        });
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('fast');
            }, 6000);
        });

        //Refresh table
        setInterval(function () {
            $( "#mytable" ).load( "index.php #mytable" );
        }, 5000);

        //Hidden div
        $(document).ready(function(){
            $("select").change(function(){
                $( "select option:selected").each(function(){
                    if($(this).attr("value")=="document"){
                        $(".document").show();
                        $(".text").hide();
                    }else {
                        $(".document").hide();
                        $(".text").show();
                    }
                });
            }).change();
        });

    })
</script>
</body>

</html>