<?php
require_once 'agm/agm.php';
// Initialize the session
session_start();

//minutes or more.
$expireAfter = 30;

//Check to see if our "last action" session
//variable has been set.
if(isset($_SESSION['last_action'])){

    //Figure out how many seconds have passed
    //since the user was last active.
    $secondsInactive = time() - $_SESSION['last_action'];

    //Convert our minutes into seconds.
    $expireAfterSeconds = $expireAfter * 60;

    //Check to see if they have been inactive for too long.
    if($secondsInactive >= $expireAfterSeconds){
        //User has been inactive for too long.
        //Kill their session.
        session_unset();
        session_destroy();
    }

}

//Assign the current timestamp as the user's
//latest activity
$_SESSION['last_action'] = time();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login");
    exit;
}
require_once 'agm/session.php';
date_default_timezone_set('Africa/Nairobi');//set date and time
$timeline = mysqli_fetch_assoc(mysqli_query($con, "SELECT * FROM timeline"));
$start = $timeline['starttime'];
$end = $timeline['endtime'];
$now = date("Y-m-d H:i:s");

if ($start > $now){
    $time = $timeline['starttime'];
}else{
    $time = $timeline['endtime'];
}


$members = mysqli_num_rows(mysqli_query($con, "SELECT * FROM users"));
$proxy = mysqli_num_rows(mysqli_query($con, "SELECT * FROM proxy_users"));
$shares = mysqli_fetch_assoc(mysqli_query($con, "SELECT SUM(shares) AS shares FROM users")); //total shares
$users = mysqli_num_rows(mysqli_query($con, "SELECT * FROM users WHERE pin !== NULL"));

$sql = mysqli_fetch_assoc(mysqli_query($con, "select COUNT(date_sub(updated, interval 30 day)) as new FROM users"));
$perc_users = $sql['new']/$members * 100;

$sql1 = mysqli_fetch_assoc(mysqli_query($con, "select COUNT(date_sub(updated, interval 30 day)) as new FROM proxy_users"));
$perc_proxy_users = $sql1['new']/$proxy * 100;

$max = mysqli_fetch_assoc(mysqli_query($con, "select MAX(shares) as new FROM shares"));

$active = mysqli_fetch_assoc(mysqli_query($con, "select COUNT(phoneNo) as active FROM session_levels WHERE date_sub(created, interval 7 day)"));
$active_3 = mysqli_fetch_assoc(mysqli_query($con, "select COUNT(phoneNo) as active FROM session_levels WHERE date_sub(created, interval 30 day)"));

$sql2 = mysqli_query($con, "SELECT agenda_name, agenda_question, vote, COUNT(vote) AS total FROM votes GROUP BY agenda_name, agenda_question, vote ORDER BY total DESC");//get total answers
$answers = mysqli_fetch_assoc($sql2);

$sql3 = mysqli_query($con, "SELECT agenda_name, COUNT(voter_MemberNo) AS total FROM votes GROUP BY agenda_name ORDER BY total DESC");
$votes = mysqli_fetch_assoc($sql3);

$query8 = mysqli_query($con, "SELECT attendance.phoneNo, attendance.MemberNo, users.shares, attendance.created FROM attendance INNER JOIN users ON attendance.MemberNo = users.MemberNo ORDER BY attendance.created ASC");
$attendance = mysqli_fetch_assoc($query8);
$total_attendance = mysqli_num_rows($query8);

$total_attendance_shares = mysqli_fetch_assoc(mysqli_query($con, "SELECT SUM(users.shares) AS total FROM attendance INNER JOIN users ON attendance.MemberNo = users.MemberNo"));


$percentage = $total_attendance_shares['total'] / $shares['shares']*100;

$totalVote = mysqli_num_rows(mysqli_query($con, "select * from votes"));
$perc_votes = $totalVote / $members * 100;

$registered = mysqli_num_rows(mysqli_query($con, "select * from users where pin is not null"));
$sql5 = mysqli_fetch_assoc(mysqli_query($con, "select COUNT(date_sub(updated, interval 7 day)) as new FROM users where pin is not null"));
$perc_reg = $sql5['new']/$registered * 100;

$query13 = mysqli_query($con, "SELECT agenda_no, SUM(shares) as total_shares FROM votes GROUP BY agenda_no ORDER BY agenda_no DESC LIMIT 1");
$total = mysqli_fetch_assoc(mysqli_query($con, "SELECT agenda_no, COUNT(agenda_no) as total FROM votes GROUP BY agenda_no ORDER BY agenda_no DESC LIMIT 1"));
$total_votes = mysqli_fetch_assoc($query13);

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | HOME</title>
  <!-- Favicon -->
  <link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <!-- Argon CSS -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">

</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="agenda">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="members">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="proxy">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="attendance">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="advanced">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="cards">
    <!-- Topnav -->
      <nav class="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
          <div class="container-fluid">
              <div class="collapse navbar-collapse" id="navbarSupportedContent">

                  <form class="navbar-search form-inline mr-sm-3">
                      <div class="form-group mb-0">
                          <div class="input-group input-group-alternative input-group-merge">
                              <div class="input-group-prepend">
                                  <img src="assets/img/DTB_Logo-01_9.png">
                              </div>
                          </div>
                      </div>
                  </form>
                  <!-- Navbar links -->
                  <ul class="navbar-nav align-items-center  ml-md-auto ">
                      <li class="nav-item d-xl-none">
                          <!-- Sidenav toggler -->
                          <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                              <div class="sidenav-toggler-inner">
                                  <i class="sidenav-toggler-line"></i>
                                  <i class="sidenav-toggler-line"></i>
                                  <i class="sidenav-toggler-line"></i>
                              </div>
                          </div>
                      </li>
                  </ul>
                  <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
                      <li class="nav-item dropdown">
                          <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="https://via.placeholder.com/150">
                  </span>
                                  <div class="media-body  ml-2  d-none d-lg-block">
                                      <span class="mb-0 text-sm  font-weight-bold"><?php echo $first_name," ".$last_name; ?></span>
                                  </div>
                              </div>
                          </a>
                          <div class="dropdown-menu  dropdown-menu-right ">
                              <div class="dropdown-header noti-title">
                                  <h6 class="text-overflow m-0">Welcome!</h6>
                              </div>
                              <a href="logout.php" class="dropdown-item">
                                  <i class="ni ni-user-run"></i>
                                  <span>Logout</span>
                              </a>
                          </div>
                      </li>
                  </ul>
              </div>
          </div>
      </nav>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Default</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Default</li>
                </ol>
              </nav>
            </div>
          </div>
          <!-- Card stats -->
          <div class="row">
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Members</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $members; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-user-run"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo $perc_users; ?>%</span>
                    <span class="text-nowrap">Last 1 month</span>
                  </p>
                </div>
              </div>
            </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card card-stats">
                      <!-- Card body -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col">
                                  <h5 class="card-title text-uppercase text-muted mb-0">Registered</h5>
                                  <span class="h2 font-weight-bold mb-0"><?php echo $registered; ?></span>
                              </div>
                              <div class="col-auto">
                                  <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow">
                                      <i class="ni ni-collection"></i>
                                  </div>
                              </div>
                          </div>
                          <p class="mt-3 mb-0 text-sm">
                              <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo round($perc_reg, 2); ?> %</span>
                              <span class="text-nowrap">Last 7 days</span>
                          </p>
                      </div>
                  </div>
              </div>
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Proxy</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $proxy; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                        <i class="ni ni-user-run"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i><?php echo $perc_proxy_users; ?>%</span>
                    <span class="text-nowrap">Last 1 month</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Shares</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo $shares['shares']; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i class="ni ni-money-coins"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i><?php echo $max['new']; ?></span>
                    <span class="text-nowrap">Highest share</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-2 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <h5 class="card-title text-uppercase text-muted mb-0">Attending</h5>
                      <span class="h2 font-weight-bold mb-0"><?php echo round($percentage, 2)."%"; ?></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  <p class="mt-3 mb-0 text-sm">
                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo $total_attendance; ?></span>
                    <span class="text-nowrap">Attending Members</span>
                  </p>
                </div>
              </div>
            </div>
              <div class="col-xl-2 col-md-6">
                  <div class="card card-stats">
                      <!-- Card body -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col">
                                  <h5 class="card-title text-uppercase text-muted mb-0">vote count</h5>
                                  <span class="h2 font-weight-bold mb-0"><?php echo $total_votes['total_shares']; ?></span>
                              </div>
                              <div class="col-auto">
                                  <div class="icon icon-shape bg-gradient-purple text-white rounded-circle shadow">
                                      <i class="ni ni-active-40"></i>
                                  </div>
                              </div>
                          </div>
                          <p class="mt-3 mb-0 text-sm">
                              <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo $total['total']; ?></span>
                              <span class="text-nowrap">Votes (Members) </span>
                          </p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row" style="padding-bottom: 20px">
          <div class="col-xl-8 order-1">
              <div class="card">
                  <div class="card-header border-0">
                      <div class="row align-items-center">
                          <div class="col">
                              <h3 class="mb-0">Voting<div class="text-right"><button type="button" class="text-right btn btn-primary btn-sm" id="csv">Download</button></div></h3>
                          </div>
                      </div>
                  </div>
                  <div class="table-responsive">
                      <!-- Projects table -->
                      <table class="table align-items-center table-flush" id="dashboard">
                          <thead class="thead-light">
                          <tr>
                              <th scope="col">Agenda</th>
                              <th scope="col">Question</th>
                              <th scope="col">Answers</th>
                              <th scope="col" class="text-center">Votes</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php do{ ?>
                          <tr>
                              <td><?php echo $answers['agenda_name']; ?></td>
                              <td><?php echo $answers['agenda_question']; ?></td>
                              <td><?php echo $answers['vote']; ?></td>
                              <td class="text-center"><?php echo $answers['total']; ?></td>
                          </tr>
                          <tr>
                          </tr>
                          <?php } while($answers = mysqli_fetch_assoc($sql2)); ?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
          <div class="col-xl-4 order-2">

              <!-- Display the countdown timer in an element -->
              <div class="col-12">
                  <div class="card card-stats">
                      <!-- Card body -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col">
                                  <?php if ($start > $now){ ?>
                                  <h5 class="card-title text-uppercase text-muted mb-0">Meeting Starting In</h5>
                                  <?php } else { ?>
                                  <h5 class="card-title text-uppercase text-muted mb-0">Meeting Ending In</h5>
                                  <?php } ?>
                                  <p></p>
                                  <span class="h2 font-weight-bold mb-0"><h1 class="text-success" id="demo"></h1></span>
                              </div>
                              <div class="col-auto">
                                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                      <i class="ni ni-time-alarm"></i>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

              <!-- Display quorum status -->
              <div class="col-12">
                  <div class="card card-stats">
                      <!-- Card body -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col">
                                  <h5 class="card-title text-uppercase text-muted mb-0">quorum status (Attendance) </h5>
                                  <p></p>
                                  <span class="h2 font-weight-bold mb-0">
                                      <?php if ($percentage >= 50){ echo "ACHIEVED"; }else{ echo "NOT YET ACHIEVED"; } ?>
                                  </span>
                              </div>
                              <div class="col-auto">
                                  <div class="icon icon-shape bg-gradient-purple text-white rounded-circle shadow">
                                      <i class="ni ni-bell-55"></i>
                                  </div>
                              </div>
                          </div>
                          <p class="mt-3 mb-0 text-sm">
                              <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo round($percentage,2); ?> % </span>
                              <span class="text-nowrap">Currently</span>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-12">
                  <div class="card card-stats">
                      <!-- Card body -->
                      <div class="card-body">
                          <div class="row">
                              <div class="col">
                                  <h5 class="card-title text-uppercase text-muted mb-0">quorum status (Voting) </h5>
                                  <p></p>
                                  <span class="h2 font-weight-bold mb-0">
                                      <?php if ($percentage >= 50 && $perc_votes >= 50){ echo "ACHIEVED"; }else{ echo "NOT YET ACHIEVED"; } ?>
                                  </span>
                              </div>
                              <div class="col-auto">
                                  <div class="icon icon-shape bg-gradient-yellow text-white rounded-circle shadow">
                                      <i class="ni ni-bell-55"></i>
                                  </div>
                              </div>
                          </div>
                          <p class="mt-3 mb-0 text-sm">
                              <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> <?php echo round($perc_votes,2); ?> %</span>
                              <span class="text-nowrap">Currently</span>
                          </p>
                      </div>
                  </div>
              </div>

          </div>
          <div class="col-xl-8 order-3">
              <div class="card">
                  <div class="card-header border-0">
                      <div class="row align-items-center">
                          <div class="col">
                              <h3 class="mb-0">Voting Statistics<div class="text-right"><button type="button" class="text-right btn btn-primary btn-sm" id="excel">Download</button></div></h3>
                          </div>
                      </div>
                  </div>
                  <div class="table-responsive">
                      <!-- Projects table -->
                      <table class="table align-items-center table-flush" id="dashboard2">
                          <thead class="thead-light">
                          <tr>
                              <th scope="col">Agenda Name</th>
                              <th scope="col" class="text-center">Members voted</th>
                              <th scope="col" class="text-center">Pending votes</th>
                          </tr>
                          </thead>
                          <tbody>
                          <?php do{ ?>
                              <tr>
                                  <th scope="row"><?php echo $votes['agenda_name']; ?></th>
                                  <td class="text-center"><?php echo $votes['total']; ?></td>
                                  <td class="text-center"><?php echo $members - $votes['total']; ?></td>
                              </tr>
                              <tr>
                              </tr>
                          <?php } while($votes = mysqli_fetch_assoc($sql3)); ?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
      <!-- Footer -->
        <?php include 'partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
  <script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Argon JS -->
  <script src="assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>

  <!-- Export data -->
  <script src="assets/js/main.js"></script>

  <script>
    $(document).ready(function (e) {
        //Refresh data
        setInterval(function () {
            $( "#cards" ).load( "index.php #cards" );
        }, 5000);
    })
  </script>

  <!-- Export data -->
  <script>
      $('#csv').on('click',function(){
          $("#dashboard").tableHTMLExport({type:'csv',filename:'statistics.csv'});
      });

      $('#excel').on('click',function(){
          $("#dashboard2").tableHTMLExport({type:'csv',filename:'vote.csv'});
      });

      $('#attendance').on('click',function(){
          $("#dashboard3").tableHTMLExport({type:'csv',filename:'attendance.csv'});
      });
  </script>

  <!-- Display the countdown timer in an element -->
  <script>
      // Set the date we're counting down to
      var countDownDate = new Date("<?php echo $time; ?>").getTime();

      // Update the count down every 1 second
      var x = setInterval(function() {

          // Get today's date and time
          var now = new Date().getTime();

          // Find the distance between now and the count down date
          var distance = countDownDate - now;

          // Time calculations for days, hours, minutes and seconds
          var days = Math.floor(distance / (1000 * 60 * 60 * 24));
          var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
          var seconds = Math.floor((distance % (1000 * 60)) / 1000);

          // Display the result in the element with id="demo"
          document.getElementById("demo").innerHTML = days + "d " + hours + "h "
              + minutes + "m " + seconds + "s ";

          // If the count down is finished, write some text
          if (distance < 0) {
              clearInterval(x);
              document.getElementById("demo").innerHTML = "EXPIRED";
          }
      }, 1000);
  </script>
</body>

</html>