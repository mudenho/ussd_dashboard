<?php
include '../agm/agm.php';

require_once '../vendor/autoload.php';

// username and password sent from form
$email = mysqli_real_escape_string($con, $_POST['email']);

$sql = "SELECT * FROM admins WHERE email = '$email'";
$result_login = mysqli_query($con, $sql);
$code = mysqli_fetch_assoc($result_login);

$count = mysqli_num_rows($result_login);

if ($count == 1) {

    //functions
    function Unique($no){
        $Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789';
        $QuantidadeCaracteres = strlen($Caracteres);
        $QuantidadeCaracteres--;

        $Hash=NULL;
        for($x=1;$x<=$no;$x++){
            $Posicao = rand(0,$QuantidadeCaracteres);
            $Hash .= substr($Caracteres,$Posicao,1);
        }

        return $Hash;
    }

    function sendMail($receiver, $subject, $body){
        // Create the Transport
        $transport = (new Swift_SmtpTransport('smtp.gmail.com', 465, 'ssl'))
            ->setUsername('ocholarosemary19@gmail.com')
            ->setPassword('hvhzqfiqlrigrdyg')
        ;

// Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

// Create a message
        $message = (new Swift_Message($subject))
            ->setFrom(['ocholarosemary19@gmail.com' => 'AGM Campaign'])
            ->setTo([$receiver => $receiver])
            ->setBody($body)
        ;

        // Send the message
        return $mailer->send($message);
    }

    //generate reset code
    $reset_code = Unique(6);
    $admins_id = $code['id'];

    //insert into db
    $sql = "INSERT INTO reset_password (admins_id, email, reset_code, created) VALUES ('$admins_id', '$email', '$reset_code', now())";
    $result_code = mysqli_query($con, $sql);

    //send code to email
    sendMail($email, "Password Reset", "You reset your password, use this reset code: $reset_code");


}

?>


<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | RESET PASSWORD</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body class="bg-default">
  <!-- Main content -->
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
              <h1 class="text-white">Reset Password</h1>
              <p class="text-lead text-white">AGM CAMPAIGN MANAGEMENT DASHBOARD</p>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>

    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
            <div class="">
                <?php if (isset($_GET['status']) && $_GET['status'] == 'message'){ ?>
                    <!-- success message-->
                    <div class="content-center" style="text-align: center;">
                        <div class="statusMsg">
                            <p class="alert alert-success"><b>We have sent a reset code to your email address if it exists. Please enter the reset code to reset your password.</b></p>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-12">
                    <!--Message Response-->
                    <div class="message"></div>
                </div>
            </div>
          <div class="card bg-secondary border-0 mb-0">
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Reset Password</small>
              </div>
              <form class="reset" enctype="multipart/form-data">
                <div class="form-group mb-3">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-mobile-button"></i></span>
                    </div>
                    <input class="form-control" placeholder="Reset Code" type="text" name="reset_code" autocomplete="off">
                  </div>
                </div>
                  <div class="form-group mb-3">
                      <div class="input-group input-group-merge input-group-alternative">
                          <div class="input-group-prepend">
                              <span class="input-group-text"><i class="ni ni-laptop"></i></span>
                          </div>
                          <input class="form-control" placeholder="New Password" type="password" name="password">
                      </div>
                  </div>
                  <div class="form-group mb-3">
                      <div class="input-group input-group-merge input-group-alternative">
                          <div class="input-group-prepend">
                              <span class="input-group-text"><i class="ni ni-laptop"></i></span>
                          </div>
                          <input class="form-control" placeholder="Confirm Password" type="password" name="confirm_password">
                      </div>
                  </div>
                <div class="text-center">
                    <input type="hidden" name="email" value="<?php echo $email; ?>">
                    <input type="hidden" name="role_id" value="100">
                    <input type="hidden" name="form" value="resetPassword">
                    <input type="submit" class="btn btn-primary my-4" value="Reset">
                </div>
              </form>
            </div>
          </div>
          <div class="row mt-3">
            <div class="col-6">
              <a href="../login" class="text-light"><small>Sign In</small></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  <footer class="py-5" id="footer-main">
    <div class="container">
      <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center  text-lg-left  text-muted">
                &copy; <script>document.write( new Date().getFullYear() );</script>
                <a href="https://www.legitimate-technology.co.ke" class="font-weight-bold ml-1" target="_blank">Legitimate Technologies</a>
            </div>
        </div>
        <div class="col-xl-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                <li class="nav-item">
                    <a href="https://www.legitimate-technology.co.ke" class="nav-link" target="_blank">Legitimate Technologies</a>
                </li>
                <li class="nav-item">
                    <a href="https://www.legitimate-technology.co.ke" class="nav-link" target="_blank">About Us</a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </footer>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>


  <!--Form Submission-->
  <script>
      $(document).ready(function (e) {
          $(".reset").on('submit', function (e) {
              e.preventDefault();
              $(".message").html("<p class='alert alert-info'>Wait... Submitting data...</p>");
              $.ajax({
                  type: 'POST',
                  url: '../agm/submit.php',
                  data: new FormData(this),
                  dataType: 'json',
                  contentType: false,
                  cache: false,
                  processData: false,
                  beforeSend: function () {
                      $(".btn-submit").attr("disabled", "disabled");
                      $(".reset").css("opacity", ".5");
                  },
                  success: function(response){ //console.log(response);
                      $('.message').html('');
                      if(response.status == 200){

                          //window.location.replace("../login/index.php?status=success");
                          $('.message').html('<p class="alert alert-success">'+response.message+'</p>');

                      }else{
                          $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                      }
                      $('.reset').css("opacity","");
                      $(".btn-submit").removeAttr("disabled");
                  }
              });
          });

      });

  </script>
</body>

</html>