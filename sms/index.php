<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | SMS</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="../">
                <i class="ni ni-tv-2 text-primary"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../agenda">
                <i class="ni ni-bullet-list-67 text-orange"></i>
                <span class="nav-link-text">Agendas</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../documents">
                <i class="ni ni-cloud-download-95 text-primary"></i>
                <span class="nav-link-text">Documents</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="../members">
                <i class="ni ni-user-run text-yellow"></i>
                <span class="nav-link-text">Members</span>
              </a>
            </li>
              <li class="nav-item">
                  <a class="nav-link" href="../database">
                      <i class="ni ni-bullet-list-67 text-yellow"></i>
                      <span class="nav-link-text">Members Database</span>
                  </a>
              </li>
            <li class="nav-item">
              <a class="nav-link" href="../proxy">
                <i class="ni ni-watch-time text-default"></i>
                <span class="nav-link-text">Proxy Users</span>
              </a>
            </li>
              <li class="nav-item">
                  <a class="nav-link" href="../attendance">
                      <i class="ni ni-archive-2 text-default"></i>
                      <span class="nav-link-text">Attendance</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../voting">
                      <i class="ni ni-collection text-default"></i>
                      <span class="nav-link-text">Voting</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../faq">
                      <i class="ni ni-book-bookmark text-default"></i>
                      <span class="nav-link-text">FAQs</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link active" href="javascript:void(0)">
                      <i class="ni ni-send text-primary"></i>
                      <span class="nav-link-text">Send SMS</span>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="../advanced">
                      <i class="ni ni-single-02 text-default"></i>
                      <span class="nav-link-text">Advanced</span>
                  </a>
              </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">SMS</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">SMS</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
          <?php if ($role_id == 100 || $role_id == 90 || $role_id == 50){ ?>
          <div class='col-xl-8 order-xl-1'>
          <div class='card'>
            <div class='card-header'>
              <div class='row align-items-center'>
                  <div class='col-12'>
                      <!--Message Response-->
                      <div class='message'></div>
                  </div>
                <div class='col-8'>
                  <h3 class='mb-0'>Send SMS </h3>
                </div>
              </div>
            </div>
            <div class='card-body'>
              <form id='sms' enctype='multipart/form-data'>
                <div class='pl-lg-4'>
                  <div class='row bg-secondary'>
                      <div class='col-lg-12'>
                          <div class='form-group'>
                              <label class='form-control-label' for='receivers'>Recipients</label>
                              <select class='form-control form-control-alternative' name='receivers'>
                                  <option value=''>Choose Recipients</option>
                                  <option value='membersProxy'>All Members(with Proxy Users)</option>
                                  <option value='members'>All Members(without Proxy users)</option>
                                  <option value='proxy'>Proxy Users Only</option>
                                  <option value='attenders'>Attended Members</option>
                                  <option value='voters'>Voters</option>

                              </select>
                          </div>
                      </div>
                      <div class='col-lg-12 text'>
                          <div class='form-group'>
                              <label class='form-control-label' for='text'>Enter message to send</label>
                              <textarea name='message' rows='8' class='form-control form-control-alternative' onkeyup="countChar(this)" placeholder="Be aware that a message of more than 160 characters will be splitted and send in bunches"></textarea>
                          </div>
                          <div id="charNum" class="text-red text-right"></div>
                      </div>
                  </div>
                    <div class='row'>
                        <input type='hidden' name='role_id' value="<?php echo $role_id; ?>">
                        <input type='hidden' name='uploader' value="<?php echo $last_name," ".$first_name; ?>">
                        <input type='hidden' name='form' value='sms'>
                        <input type='submit' class='btn btn-primary btn-submit' value='Send'>
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
          <?php } ?>
      </div>
        <div class="row" style="padding-bottom: 20px">
            <div class="col-12">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0 text-white">Message History</h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark table-flush" id="mytable">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="sort text-center" data-sort="name">Recipients</th>
                                <th scope="col" class="sort" data-sort="id">Message</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php do{ ?>
                                <tr>
                                    <td class="text-center"><?php echo $sms['total']; ?></td>
                                    <td ><?php echo $sms['message']; ?></td>
                                </tr>
                            <?php } while($sms = mysqli_fetch_assoc($sql1)); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <!--Count Characters-->
  <script>
      function countChar(val) {
          var len = val.value.length;
          if (len >= 480) {
              val.value = val.value.substring(0, 480);
          } else {
              $('#charNum').text(len + " Characters ( Remaining " + (160 - len) + ")");
          }
      }
  </script>

  <!--Form Submission-->
  <script>
      $(document).ready(function (e) {
          $("#sms").on('submit', function (e) {
              e.preventDefault();
              $(".message").html("<p class='alert alert-info'>Wait... Sending SMS...</p>");
              $.ajax({
                  type: 'POST',
                  url: '../agm/submit.php',
                  data: new FormData(this),
                  dataType: 'json',
                  contentType: false,
                  cache: false,
                  processData: false,
                  beforeSend: function () {
                      $(".btn-submit").attr("disabled", "disabled");
                      $("#sms").css("opacity", ".5");
                  },
                  success: function(response){ //console.log(response);
                      $('.message').html('');
                      if(response.status == 200){
                          $('#sms')[0].reset();
                          $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                      }else{
                          $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                      }
                      $('#sms').css("opacity","");
                      $(".btn-submit").removeAttr("disabled");
                  }
              });
          });

          //Refresh table
          setInterval(function () {
              $( "#mytable" ).load( "index.php #mytable" );
          }, 6000);

      })
  </script>
</body>

</html>