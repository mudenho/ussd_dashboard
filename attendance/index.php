<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | ATTENDANCE</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../agenda">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../members">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Add Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../proxy">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="javascript:void(0)">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../advanced">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Attendance</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Attendance</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
        <div class="row" style="padding-bottom: 20px">
            <div class="col-12">
                <div class="card bg-default shadow">
                    <div class="card-header bg-transparent border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0 text-white">In Attendance<div class="text-right"><button type="button" class="text-right btn btn-primary btn-sm" id="csv">Download</button></div></h3>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-dark table-flush" id="dataTable">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" class="text-center">Full Names</th>
                                <th scope="col" class="text-center">Phone Number</th>
                                <th scope="col" class="text-center">Member No</th>
                                <th scope="col" class="text-center">Shares Held</th>
                                <th scope="col" class="text-center">Membership Type</th>
                                <th scope="col" class="text-center">Proxy's Name (if any)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php do{ ?>
                                <tr>
                                    <td class="text-center"><?php echo $attendance['user']; ?></td>
                                    <td class="text-center"><?php echo $attendance['phoneNo']; ?></td>
                                    <td class="text-center"><?php echo $attendance['MemberNo']; ?></td>
                                    <td class="text-center"><?php echo $attendance['shares']; ?></td>
                                    <td class="text-center"><?php echo $attendance['type']; ?></td>
                                    <td class="text-center"><?php echo $attendance['proxy']; ?></td>
                                </tr>
                            <?php } while($attendance = mysqli_fetch_assoc($query8)); ?>
                            </tbody>
                            <tfoot class="">
                            <tr>
                                <th scope="col" class="text-center text-white">Total Members: <?php echo $total_members; ?></th>
                                <th scope="col" class="text-center text-white">Attending Members: <?php echo $total_attendance; ?></th>
                                <th scope="col" class="text-center text-white">Total Shares: <?php echo $shares['shares']; ?> (100%)</th>
                                <th scope="col" colspan="2" class="text-center text-white">Attending Shares: <?php echo $total_attendance_shares['total']." (".round($percentage, 2)."%)"; ?></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>
  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!-- Export data -->
  <script src="../assets/js/main.js"></script>

  <!-- Export data -->
  <script>
      $('#csv').on('click',function(){
          $("#dataTable").tableHTMLExport({type:'csv',filename:'attendance.csv'});
      });
      $('#pdf').on('click',function(){
          $("#dataTable").tableHTMLExport({type:'pdf',orientation: 'l',filename:'school.pdf'});
      })
  </script>
</body>

</html>