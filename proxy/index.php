<?php

require '../agm/backend.php';

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>AGM | PROXY USERS</title>
  <!-- Favicon -->
  <link rel="icon" href="../assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="../assets/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../assets/css/argon.css?v=1.2.0" type="text/css">
</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="http://legitimate-technology.co.ke">
                <img src="../assets/img/blue.png" class="navbar-brand-img" alt="...">
            </a>
        </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../">
                        <i class="ni ni-tv-2 text-primary"></i>
                        <span class="nav-link-text">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../agenda">
                        <i class="ni ni-bullet-list-67 text-orange"></i>
                        <span class="nav-link-text">Agendas</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../documents">
                        <i class="ni ni-cloud-download-95 text-primary"></i>
                        <span class="nav-link-text">Documents</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../members">
                        <i class="ni ni-user-run text-yellow"></i>
                        <span class="nav-link-text">Members</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../database">
                        <i class="ni ni-bullet-list-67 text-yellow"></i>
                        <span class="nav-link-text">Members Database</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="#">
                        <i class="ni ni-watch-time text-default"></i>
                        <span class="nav-link-text">Proxy Users</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../attendance">
                        <i class="ni ni-archive-2 text-default"></i>
                        <span class="nav-link-text">Attendance</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../voting">
                        <i class="ni ni-collection text-default"></i>
                        <span class="nav-link-text">Voting</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../faq">
                        <i class="ni ni-book-bookmark text-default"></i>
                        <span class="nav-link-text">FAQs</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../sms">
                        <i class="ni ni-send text-primary"></i>
                        <span class="nav-link-text">Send SMS</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../advanced">
                        <i class="ni ni-single-02 text-default"></i>
                        <span class="nav-link-text">Advanced</span>
                    </a>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include '../partials/navbar.php'; ?>
    <!-- Header -->
    <!-- Header -->
      <div class="header bg-primary pb-6">
          <div class="container-fluid">
              <div class="header-body">
                  <div class="row align-items-center py-4">
                      <div class="col-lg-6 col-7">
                          <h6 class="h2 text-white d-inline-block mb-0">Proxy Users</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                              <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                  <li class="breadcrumb-item"><a href="#">Dashboards</a></li>
                                  <li class="breadcrumb-item active" aria-current="page">Proxy Users</li>
                              </ol>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col-xl-12 order-xl-2">
            <div class="card bg-default shadow">
                <div class="card-header bg-transparent border-0">
                    <div class="text-right"><button type="button" class="btn btn-primary btn-sm" id="csv">Download</button></div>
                </div>
                <div class="table-responsive">
                    <div id="mytable">
                        <?php if (count($proxy['id']) > 0){ ?>
                            <table class="table align-items-center table-dark">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" class="sort" data-sort="id">ID</th>
                                    <th scope="col" class="sort" data-sort="users_MemberNo">Creator Membership No</th>
                                    <th scope="col" class="sort" data-sort="phoneNo">Phone No</th>
                                    <th scope="col" class="sort" data-sort="phoneNo">Full Name</th>
                                    <th scope="col" class="sort" data-sort="ID_RegCert_No">ID</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                <?php do{ ?>
                                    <tr>
                                        <td class="id"><?php echo $proxy['id']; ?></td>
                                        <td class="users_MemberNo"><?php echo $proxy['users_MemberNo']; ?></td>
                                        <td class="phoneNo"><?php echo $proxy['phoneNo']; ?></td>
                                        <td class="phoneNo"><?php echo $proxy['full_name']; ?></td>
                                        <td class="ID_RegCert_No"><?php echo $proxy['ID_RegCert_No']; ?></td>
                                        <td class="text-right">
                                            <form class="del" enctype="multipart/form-data">
                                                <input type="hidden" name="form" value="DelProxy">
                                                <input type="hidden" name="role_id" value="<?php echo $role_id; ?>">
                                                <input type="hidden" name="id" value="<?php echo $proxy['id']; ?>">
                                                <input type="submit" class='btn btn-sm btn-outline-danger' value="Delete">
                                            </form>
                                        </td>
                                    </tr>
                                <?php } while ($proxy = mysqli_fetch_assoc($query3)); ?>
                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
      </div>
      <!-- Footer -->
        <?php include '../partials/footer.php'; ?>
    </div>
  </div>

  <!-- Argon Scripts -->
  <!-- Core -->
  <script src="../assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/js-cookie/js.cookie.js"></script>
  <script src="../assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Argon JS -->
  <script src="../assets/js/argon.js?v=1.2.0"></script>
  <!--Ajax-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
  <!-- Export data -->
  <script src="../assets/js/main.js"></script>

  <!--Form Submission-->
<script>
    $(document).ready(function (e) {
        $(".members").on('submit', function (e) {
            e.preventDefault();
            $(".message").html("<p class='alert alert-info'>Wait... Submitting data...</p>");
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                    $(".btn-submit").attr("disabled", "disabled");
                    $(".members").css("opacity", ".5");
                },
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        $('.members')[0].reset();
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                    $('.members').css("opacity","");
                    $(".btn-submit").removeAttr("disabled");
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('slow');
            }, 30000);
        })
        // Delete Button
        $(".del").on('submit', function (e) {
            e.preventDefault();
            var el = this;
            $.ajax({
                type: 'POST',
                url: '../agm/submit.php',
                data: new FormData(this),
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,
                success: function(response){ //console.log(response);
                    $('.message').html('');
                    if(response.status == 200){
                        // Remove row from HTML Table
                        $(el).closest('tr').css('background','tomato');
                        $(el).closest('tr').fadeOut(800,function(){
                            $(this).remove();
                        });
                        $('.message').html('<p class="alert alert-success">'+response.message+'</p>');
                    }else{
                        $('.message').html('<p class="alert alert-danger">'+response.message+'</p>');
                    }
                }
            });
            setTimeout(function() {
                $('.message').fadeOut('fast');
            }, 3000);
        })

        //Refresh table
        setInterval(function () {
            $( "#mytable" ).load( "index.php #mytable" );
        }, 5000);

    })
</script>
  <!-- Export data -->
  <script>
      $('#csv').on('click',function(){
          $("#dataTable").tableHTMLExport({type:'csv',filename:'Members.csv'});
      });
      $('#pdf').on('click',function(){
          $("#dataTable").tableHTMLExport({type:'pdf',orientation: 'l',filename:'school.pdf'});
      })
  </script>
</body>

</html>